exports = module.exports = {

  // Full documentation available at 'https://bitbucket.org/mtigau/gz-siem-poc'

  /*
   * Use the Bitdefender GravityZone Control Center (https://cloud.gravityzone.bitdefender.com)
   * to get the Control Center API Access URL and to generate an API key.
   */
  gravityzoneControlCenterAPIURL: 'https://cloud.gravityzone.bitdefender.com/api',
  apiKey: 'BITDEFENDER_GZ_API_KEY',

  /*
   * Usually a reverse proxy or a Firewall rule is required
   * to make `customerExternalURL` accessible from internet
   * so Bitdefender GravityZone can push the events.
   */
  customerExternalURL: 'https://my-company-external-accessible-URL.where-events-should-be.pushed',

  /*
   * Activate or deactivate Bitdefender GravityZone Event Push Service.
   * Restart the app to apply the settings.
   */
  enableEventPushServiceAPI: true,

  // local port to be used by the application (not the external PORT)
  localPort: 3000,

  /*
   * Received events will be logged if `enabled` is `true`
   * to the logs/events.log file relative to the application folder.
   * When `path` is a custom folder, it must exists before the application starts.
   */
  logEventsToFile: {
    enabled: true,
    filename: 'events.log',
    path: './logs'
  },

  requireValidSslCertificate: true,

  subscribeToEventTypes: {
    'modules': true,
    'sva': true,
    'registration': true,
    'supa-update-status': true,
    'av': true,
    'aph': true,
    'fw': true,
    'avc': true,
    'uc': true,
    'dp': true,
    'sva-load': true,
    'task-status': true,
    'exchange-malware': true,
    'network-sandboxing': true,
    'adcloud': true,
    'exchange-user-credentials': true
  }
}
