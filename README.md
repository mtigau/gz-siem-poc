# Bitdefender GravityZone SIEM Integration Demo

This sample application is a demonstration of how a Bitdefender Gravityzone Cloud customer can leverage the Bitdefender Gravityzone API to integrate Bitdefender Endpoints events with external SIEM platforms such as: AlienVault, Splunk, etc.

## Installation

### Prerequisites
- [Git](https://git-scm.com/downloads)
- [Node.js 4+](https://nodejs.org/en/download/)
- A Bitdefender GravityZone Partner account.

### Obtain an API Key

Get your API Key from the [Bitdefender GravityZone Control Center](https://cloud.gravityzone.bitdefender.com/).

### Install Application

Clone this repo:

    git clone git@bitbucket.org:mtigau/gz-siem-poc.git

Install the dependencies:

    cd gz-siem-poc
    npm install


## Quick Start

Edit the main configuration file `config/config.js` and set the Bitdefender GravityZone Control Center API access URL.
> US customers use `https://cloud.gravityzone.bitdefender.com`

> EU customers use `https://cloudgz.gravityzone.bitdefender.com`

Set your `BITDEFENDER_GZ_API_KEY`

Set your company **external URL** where the events will be pushed. Make sure this URL is proxied / forwarded to the local machine where this app runs.

Start the local application:

    node index.js

Use the [eicar test file](http://www.eicar.org/86-0-Intended-use.html) on one of the machines protected by Bitdefender Endpoint Client to generate a malware event which should appear in the Control Center Dashboard and also show up in the default events log file `logs/events.log`.


## Advanced Configuration

This application uses the Bitdefender Event Push Service which API is publicly available at [Bitdefender Control Center API Documentation](https://download.bitdefender.com/SMB/Cloud/GravityZone/en_US/Bitdefender_ControlCenter_API-Guide_enUS.pdf).

> The application has to be restarted to apply any configuration changes.

Set `enableEventPushServiceAPI` as `true` or `false` to enable or disable events pushing:

    { 
        ...
        enableEventPushServiceAPI: false
        ...
    }

Disable logging to file or change the default logging location:
    
    { 
        ...
        logEventsToFile: {
            enabled: true,
            filename: 'events.log',
            path: './logs'
        },
        ...
    }

Disable the validation of your external URL SSL certificate:

    { 
        ...
        requireValidSslCertificate: false
        ...
    }

Configure which events types should be pushed. See the [Bitdefender Control Center API Documentation](https://download.bitdefender.com/SMB/Cloud/GravityZone/en_US/Bitdefender_ControlCenter_API-Guide_enUS.pdf) for complete information regarding the events types and modules:

    {
        ...
        subscribeToEventTypes: {
            'modules': true,
            'av': true,
            'aph': true,
            'fw': true,
            ...
        }
    }

## Running under a process manager

We recommend running the application under a process manager such as [PM2](https://www.npmjs.com/package/pm2) or [Forever](https://www.npmjs.com/package/forever).

[Quick start with PM2](http://pm2.keymetrics.io/docs/usage/quick-start)
    
    npm install pm2 -g
    pm2 start index.js
    pm2 logs

## Log rotation and SIEM integration

This demo application does not perform any management of the log file where the events are written. This is specifically done so the customers can select the best option to control the log file, depending of the integration with various SIEMs.

However, to prevent uncontrolled file growth in a production environment, we recommend setting up a log rotation mechanism. 
Great documentation is available from [Digital Ocean](https://www.digitalocean.com/community/tutorials/how-to-manage-log-files-with-logrotate-on-ubuntu-12-10).


## About

License: [MIT](https://bitbucket.org/mtigau/gz-siem-poc/src/master/LICENSE)

Author: Mugur Tigau

