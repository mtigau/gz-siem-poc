const pck = require('./package.json')
const config = require('./config/config')
const eventsListner = require('./lib/events-listner')

const gzAPIServiceName = "'GravityZone Event Push Service'"

const request = require('request')

const setPushEventSettings = function (config) {
  return new Promise(function (resolve, reject) {
    const payload = {
      id: '-',
      jsonrpc: '2.0',
      method: 'setPushEventSettings',
      params: {
        serviceType: 'jsonRPC',
        status: config.enableEventPushServiceAPI ? 1 : 0,
	serviceSettings:{
		url: config.customerExternalURL,
        	requireValidSslCertificate: config.requireValidSslCertificate
	},
        subscribeToEventTypes: config.subscribeToEventTypes
      }
    }

    request({
      uri: config.gravityzoneControlCenterAPIURL + '/v1.0/jsonrpc/push',
      method: 'POST',
      auth: {
        username: config.apiKey,
        password: ''
      },
      headers: {
        'Content-Type': 'application/json'
      },
      json: payload
    }, function (err, msg, rsp) {
      if (err) {
        return reject(err)
      }
      if (rsp.error) {
        return reject(new Error(rsp.error.data.details))
      }
      if (rsp.result) {
        return resolve()
      }

      return reject(new Error('failed to communicate with the GravityZone EventsPush service'))
    })
  })
}

/* Main application entry point */

console.log(`starting ${pck.name}, version ${pck.version}`)

setPushEventSettings(config)
  .then(function () {
    if (config.enableEventPushServiceAPI) {
      console.log(`${gzAPIServiceName} activated`)
      startEventsListener(config)
    } else {
      console.log(`${gzAPIServiceName} deactivated. Modify the configuration file and start the application / service to receive pushed events again.`)
      console.log(`${pck.name} will now exit.`)
    }
  }).catch(function (err) {
    console.log(err.message)
    console.log(`${pck.name} will now exit.`)
  })

const startEventsListener = function startEventsListener (config) {
  eventsListner.start(config)
}
