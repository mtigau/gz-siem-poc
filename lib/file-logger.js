var fs = require('fs')
var path = require('path')

var Log = exports = module.exports = function Log (options) {
  if (!options.enabled) {
    return
  }
  this.options = options

  var logFile = path.resolve(process.cwd(), options.path, options.filename)

  this.stream = fs.createWriteStream(logFile, { flags: 'a' })

  this.stream.on('error', function () {
    console.error(`Failed to create logfile ${logFile}. Verify the path exists and is accessible.`)
    process.exit(0)
  })
}

Log.prototype = {
  log: function (msg) {
    if (!this.options.enabled) {
      return
    }
    this.stream.write(
      msg + '\n'
    )
  }
}
