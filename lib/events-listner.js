const pck = require('../package.json')
const config = require('../config/config')

const http = require('http')
const Router = require('router')
const bodyParser = require('body-parser')

var FileLogger = require('../lib/file-logger')
var fileLogger = new FileLogger(config.logEventsToFile)

const router = Router()

router.use(bodyParser.json())

router.get('/', function (req, res) {
  res.setHeader('Content-Type', 'text/plain; charset=utf-8')
  res.end(`${pck.description} (${pck.name}) ver ${pck.version}\n`)
})

router.post('/', function (req, res, next) {
  // console.log(JSON.stringify(req.body))
  fileLogger.log(JSON.stringify(req.body))
  res.statusCode = 200
  res.end('')
})

const requestHandler = (req, res) => {
  router(req, res, function () {
    res.statusCode = 200
    res.end('')
  })
}
const server = http.createServer(requestHandler)

exports.start = function startServer (config) {
  const port = config.localPort
  server.listen(port, (err) => {
    if (err) {
      return console.error(err)
    }

    console.log(`waiting for events on local port ${port}`)
  })
}
